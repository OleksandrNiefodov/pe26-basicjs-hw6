"use srtict"

/*
Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту

- це функція яка належить об'єкту. Завдяки методу об'єкту, об'єкт може виконувати певні дії. 

2. Який тип даних може мати значення властивості об'єкта?

- строка
- число
- булеве значення 
- масив
- об'єкт
- функція
- null
- undefined

3. Об'єкт це посилальний тип даних. Що означає це поняття?

- у JS об'єкт є посилальним типом даних. Це означає, що коли хтось присвоює об'єкт іншій змінній, то об'єкт не копіюється, а присвоюється лише посилання на нього.

Практичні завдання
1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. 
Викличте цей метод та результат виведіть в консоль.
*/

const product = {
    name:'Steve Jobs: biography',
    price: 100,
    discount: '25%',
}

product.fullPrice = function () {

    //я спочатку використовув parseFloat для product.discount, і просто віднімав цей діскаунт від product.price але воно мені просто віднімало число, без урахування %, десь читав,
    // що parseFloat вміє строку з відсотком претворювати у відсоткове число але не спрацювало
   
    let  result = this.price - (this.price / 100) * parseFloat(this.discount);
    return console.log(result);
}

product.fullPrice();

/*
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
*/

const userGreeting = () => {

    const newUser = {
      name: prompt("What is you name?:"),
      age: prompt("How old are you?:"),
    };
  
    while (isNaN(newUser.age) || newUser.age < 0 || newUser.age > 110) {
      alert("Please, enter valid age:");
      newUser.age = prompt("How old are you?:");
    }
  
    alert(`Hello there, my name is ${newUser.name} and I'm ${newUser.age} years old`);
  
    return;
  }
  
  userGreeting();